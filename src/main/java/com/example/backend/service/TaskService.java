package com.example.backend.service;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.backend.messaging.TaskDTO;
import com.example.backend.model.Task;
import com.example.backend.repository.TaskRepository;

@Service
public class TaskService {
	
	@Autowired
	private TaskRepository taskRepository;	

	public List<TaskDTO> getAllTasksForUser(Long userId) {
		return convertToTaskDTO(taskRepository.findByTaskUserOrderByTaskIdDesc(userId));
	}

	public String addTaskForUser(Long userId, TaskDTO taskDTO) {
		taskDTO.setTaskUser(userId);
		taskRepository.save(convertToTask(taskDTO));
		return "Added Task";
	}

	public TaskDTO getTaskForUser(Long userId, Long taskId) {
		List<TaskDTO>tasksDTO = getAllTasksForUser(userId);
		TaskDTO tempTaskDTO = null;
		for (TaskDTO t : tasksDTO)
		{
			if (t.getTaskId() == taskId) tempTaskDTO = t;
		}
		return tempTaskDTO;
	}

	public String deleteTaskForUser(Long userId, Long taskId) {
		List<TaskDTO> tasks = getAllTasksForUser(userId);
		
		for (TaskDTO t : tasks) {
			if (t.getTaskId() == taskId) {
				taskRepository.delete(taskId);
				return "Task Deleted";
			}
		}
		return "Cannot find this task";
	}

	public String updateTaskForUser(Long userId, Long taskId, TaskDTO taskDTO) {
		
		Task tempTask = convertToTask(getTaskForUser(userId, taskId));
		Task task = convertToTask(taskDTO);
		task.setTaskId(taskId);
		task.setTaskUser(tempTask.getTaskUser());
		if (task.getTaskCategory() == null) {
			task.setTaskCategory(tempTask.getTaskCategory());
		}
		if (task.getTaskName() == null) {
			task.setTaskName(tempTask.getTaskName());
		}
		if (task.getTaskPriority() == null) {
			task.setTaskPriority(tempTask.getTaskPriority());
		}
		taskRepository.save(task);
		return "Task Updated";
	}
	
	//Task list to TaskDTO list
	private List<TaskDTO> convertToTaskDTO(List<Task> tasks) {
		List<TaskDTO> taskDTO = new ArrayList<TaskDTO> ();
		ModelMapper mm = new ModelMapper();
		
		for (Task t :  tasks)
		{
			taskDTO.add(mm.map(t, TaskDTO.class));
		}
		return taskDTO;
	}
	
	//TaskDTO to Task
	private Task convertToTask(TaskDTO taskDTO) {
		ModelMapper mm = new ModelMapper();
		Task task = mm.map(taskDTO, Task.class);
		return task;
	}
	
}
