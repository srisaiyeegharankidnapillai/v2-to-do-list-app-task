package com.example.backend.controller;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.example.backend.messaging.TaskDTO;
import com.example.backend.service.TaskService;

//@CrossOrigin(origins = {"${app.crossorigin.host1}", "${app.crossorigin.host2}"})
@RestController
@RequestMapping("/api-task/")
public class TaskController {
	
	@Autowired
	private TaskService taskService;
	
	@RequestMapping("/hi")
	private String getHi() {
		return "Hi Sri - Task is working on Openshift";
	}
	
	
	@RequestMapping("/users/{userId}/tasks/{taskId}")
	private TaskDTO getTaskForUser(@PathVariable Long userId, @PathVariable Long taskId)
	{	
		return taskService.getTaskForUser(userId, taskId);
	}
	
	@RequestMapping("/users/{userId}/tasks")
	private List<TaskDTO> getAllTasksForUser(@PathVariable Long userId)
	{	
		return taskService.getAllTasksForUser(userId);
	}
	
	@RequestMapping(value = "/users/{userId}/tasks", method= RequestMethod.POST)
	private String addTaskForUser(@PathVariable Long userId, @RequestBody TaskDTO taskDTO)
	{	
		//AppUserDTO userDTO = getUserById(userId);
		//CategoryDTO categoryDTO = getCategoryById(userId, taskDTO.getTaskCategory().getCategoryId());
		return taskService.addTaskForUser(userId, taskDTO);
	}
	
	@RequestMapping(value = "/users/{userId}/tasks/{taskId}", method= RequestMethod.DELETE)
	private String deleteTaskForUser(@PathVariable Long userId, @PathVariable Long taskId)
	{	
		return taskService.deleteTaskForUser(userId, taskId);
	}
	
	@RequestMapping(value = "/users/{userId}/tasks/{taskId}", method= RequestMethod.PATCH)
	private String updateTaskForUser(@PathVariable Long userId, @PathVariable Long taskId, @RequestBody TaskDTO taskDTO)
	{
		return taskService.updateTaskForUser(userId, taskId, taskDTO);
	}
	
	// API call to APP USER API
//	private AppUserDTO getUserById(Long userId)
//	{
//	    final String uri = "http://localhost:8082/api-user/users/{userId}";  
//	    Map<String, Long> params = new HashMap<String, Long>();
//	    params.put("userId", userId);
//	    
//	    RestTemplate restTemplate = new RestTemplate();
//	    AppUserDTO user = restTemplate.getForObject(uri, AppUserDTO.class, params);
//	    return user;
//	}
	
	// API call to Category API
//	private CategoryDTO getCategoryById(Long userId, Long categoryId)
//	{
//	    final String uri = "http://localhost:8083/api-category/users/{userId}/categories/{categoryId}";  
//	    Map<String, Long> params = new HashMap<String, Long>();
//	    params.put("userId", userId);
//	    params.put("categoryId", categoryId);
//	    
//	    RestTemplate restTemplate = new RestTemplate();
//	    CategoryDTO categoryDTO = restTemplate.getForObject(uri, CategoryDTO.class, params);
//	    return categoryDTO;
//	}
}
