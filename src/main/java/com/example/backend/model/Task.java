package com.example.backend.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Task {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long taskId;
	private String taskName;
	private Priority taskPriority;
	private boolean taskStatus;
	//@ManyToOne
	private Long taskCategory;
	//@ManyToOne
	private Long taskUser;
	
	
	public Task() {
	}

	public Task(Long taskId, String taskName, Priority taskPriority, Long taskCategory, boolean taskStatus,
			Long taskUser) {
		super();
		this.taskId = taskId;
		this.taskName = taskName;
		this.taskPriority = taskPriority;
		this.taskCategory = taskCategory;
		this.taskStatus = taskStatus;
		this.taskUser = taskUser;
	}

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public Priority getTaskPriority() {
		return taskPriority;
	}

	public void setTaskPriority(Priority taskPriority) {
		this.taskPriority = taskPriority;
	}

	public Long getTaskCategory() {
		return taskCategory;
	}

	public void setTaskCategory(Long taskCategory) {
		this.taskCategory = taskCategory;
	}

	public boolean isTaskStatus() {
		return taskStatus;
	}

	public void setTaskStatus(boolean taskStatus) {
		this.taskStatus = taskStatus;
	}

	public Long getTaskUser() {
		return taskUser;
	}

	public void setTaskUser(Long taskUser) {
		this.taskUser = taskUser;
	}
	
	
	
	
}
