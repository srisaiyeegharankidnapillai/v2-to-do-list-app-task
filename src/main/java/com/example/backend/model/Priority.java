package com.example.backend.model;

public enum Priority {
	Low,
	Medium,
	High
}
