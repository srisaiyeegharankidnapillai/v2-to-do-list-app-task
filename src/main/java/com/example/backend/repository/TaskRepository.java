package com.example.backend.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.backend.model.Task;

@Repository
public interface TaskRepository extends CrudRepository<Task,Long> {
	
	public List<Task> findByTaskUserOrderByTaskIdDesc(Long userId);
	//public Long countByTaskCategoryCategoryId(Long categoryId);
}
